# NYC School App

This application allows the user to view the SAT scores for schools in
New York City area. The data comes from two publicly available API services:

- https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2
- https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4

The app has a minimalist design and UX flow. It consists of two screens, the
first being a list of all the schools returned by the service, along with a
subset of details. The second screen shows the SAT scores for the selected school.
Tapping on a card on the first screen displays the SAT scores on the second
screen. The hardware back button (or gesture) and the back arrow returns the user to the
previous screen. A modal dialog will be presented to the user if there's an
issue fetching the data.

## Application Architecture

This is an Android application written entirely in Kotlin, and uses the
traditional XML layouts. The two screens consist of two Fragments managed by
the same Activity. Navigation between the two fragments is facilitated by the
Jetpack Navigation framework.

This application follows the Model-View-View Model architectural design
pattern. Both Fragments and the Activity all share the same ViewModel.

Dependency Injection is provided by Hilt. Retrofit-okHttp3-Moshi are used in
the networking layer. The asynchronous logic used to fetch the data from the remote services is
handled by Kotlin Coroutines, with the data presented to the user using LiveData observables.

## Code Quality

Code quality is provided through a combination of static analysis tools and
runtime monitoring. **KtLint** checks for style issues, while **DeteKT**
looks for code smells. Both tools are run automatically by the gradle build scripts at compile time.
Finally, **LeakCanary** is used to look for Memory Leaks in the application at runtime.