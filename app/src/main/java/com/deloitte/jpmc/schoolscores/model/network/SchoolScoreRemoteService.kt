package com.deloitte.jpmc.schoolscores.model.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolScoreRemoteService {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<School>>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSchoolScore(@Query("dbn") dbn: String): Response<List<SchoolScore>>
}
