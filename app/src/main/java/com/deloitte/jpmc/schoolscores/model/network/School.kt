package com.deloitte.jpmc.schoolscores.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class School(
    val dbn: String,
    @Json(name = "school_name")
    val schoolName: String,
    @Json(name = "overview_paragraph")
    val overviewParagraph: String? = null,
    val website: String? = null,
    @Json(name = "primary_address_line_1")
    val primaryAddressLineOne: String? = null,
    val city: String? = null,
    val zip: String? = null,
    @Json(name = "state_code")
    val stateCode: String? = null,
    val latitude: String? = null,
    val longitude: String? = null,
) {
    companion object {
        val queryParameterSchoolFilter = listOf(
            "dbn",
            "school_name",
            "overview_paragraph",
            "website",
            "primary_address_line_1",
            "city",
            "zip",
            "state_code",
            "latitude",
            "longitude",
        )
    }
}
