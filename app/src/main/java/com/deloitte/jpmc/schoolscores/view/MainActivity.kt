package com.deloitte.jpmc.schoolscores.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.deloitte.jpmc.schoolscores.R
import com.deloitte.jpmc.schoolscores.databinding.ActivityMainBinding
import com.deloitte.jpmc.schoolscores.viewmodel.SchoolScoreViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val viewModel: SchoolScoreViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)

        viewModel.displayMessageLiveData.observe(this) {
            if (it != null) {
                MaterialAlertDialogBuilder(this)
                    .setTitle(it)
                    .setPositiveButton(R.string.btnLblClose) { _, _ -> }
                    .show()
                viewModel.clearDisplayMessage()
            }
        }
        viewModel.fetchSchools()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}
