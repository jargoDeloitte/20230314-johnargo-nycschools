package com.deloitte.jpmc.schoolscores.model.repository

import com.deloitte.jpmc.schoolscores.model.network.SchoolScoreRemoteService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideSchoolScoreRepository(schoolScoreRemoteService: SchoolScoreRemoteService): SchoolScoreRepository {
        return SchoolScoreRepository(schoolScoreRemoteService)
    }
}
