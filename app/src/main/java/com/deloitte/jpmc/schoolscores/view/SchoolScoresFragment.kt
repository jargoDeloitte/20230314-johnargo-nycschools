package com.deloitte.jpmc.schoolscores.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.deloitte.jpmc.schoolscores.databinding.FragmentSchoolScoresBinding
import com.deloitte.jpmc.schoolscores.viewmodel.SchoolScoreViewModel

class SchoolScoresFragment : Fragment() {

    private lateinit var binding: FragmentSchoolScoresBinding
    private val viewModel: SchoolScoreViewModel by activityViewModels()
    private val args: SchoolScoresFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentSchoolScoresBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.schoolScoreLiveData.observe(viewLifecycleOwner) {
            binding.tvValTestTaker.text = it?.testTakerCount
            binding.tvValScoreRead.text = it?.satCriticalReadingAvgScore
            binding.tvValScoreMath.text = it?.satMathAvgScore
            binding.tvValScoreWrite.text = it?.satWritingAvgScore
        }
        binding.tvLblSchoolName.text = args.schoolName
        viewModel.fetchSchoolScore(args.dbn)
    }
}
