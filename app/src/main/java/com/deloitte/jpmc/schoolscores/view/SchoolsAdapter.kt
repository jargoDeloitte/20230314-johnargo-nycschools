package com.deloitte.jpmc.schoolscores.view

import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.deloitte.jpmc.schoolscores.databinding.ListItemSchoolBinding
import com.deloitte.jpmc.schoolscores.model.network.School

/**
 * Used to populate our recycler view with a list of schools.
 */
class SchoolsAdapter : RecyclerView.Adapter<SchoolsAdapter.SchoolsViewHolder>() {

    private var schools: List<School> = emptyList()
    private var clickHandler: ((String, String) -> Unit)? = null

    fun setData(schools: List<School>) {
        this.schools = schools
        notifyDataSetChanged()
    }

    fun setClickHandler(function: (String, String) -> Unit) {
        clickHandler = function
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {
        val binding = ListItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        holder.bindItems(schools[position])
    }

    override fun getItemCount(): Int = schools.size

    @SuppressWarnings("ForbiddenComment")
    inner class SchoolsViewHolder(private val binding: ListItemSchoolBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindItems(school: School) {
            // TODO: Switch to use DataBinding
            itemView.setOnClickListener { clickHandler?.invoke(school.schoolName, school.dbn) }
            binding.tvLblSchoolName.text = school.schoolName
            binding.tvValAddressLineOne.text = school.primaryAddressLineOne
            binding.tvValCity.text = school.city
            binding.tvValState.text = school.stateCode
            binding.tvValZip.text = school.zip
            binding.tvValWebsite.text = school.website
            Linkify.addLinks(binding.tvValWebsite, Linkify.WEB_URLS)
        }
    }
}
