package com.deloitte.jpmc.schoolscores.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deloitte.jpmc.schoolscores.R
import com.deloitte.jpmc.schoolscores.model.network.School
import com.deloitte.jpmc.schoolscores.model.network.SchoolScore
import com.deloitte.jpmc.schoolscores.model.repository.SchoolScoreRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * The primary viewmodel for the school scores application.
 *
 * @param schoolScoreRepository - Used for fetching the data
 */
@HiltViewModel
class SchoolScoreViewModel @Inject constructor(
    private val schoolScoreRepository: SchoolScoreRepository,
) : ViewModel() {

    private val _schoolsLiveData = MutableLiveData<List<School>>()
    val schoolsLiveData: LiveData<List<School>> = _schoolsLiveData

    private val _schoolScoreLiveData = MutableLiveData<SchoolScore?>()
    val schoolScoreLiveData: LiveData<SchoolScore?> = _schoolScoreLiveData

    private val _displayMessageLiveData = MutableLiveData<Int?>()
    val displayMessageLiveData: LiveData<Int?> = _displayMessageLiveData

    /**
     * Used to fetch the list of schools
     */
    fun fetchSchools() {
        viewModelScope.launch(Dispatchers.IO) {
            val schools = schoolScoreRepository.getSchools()
            Timber.i("Number of Schools: ${schools.size}")
            if (schools.isEmpty()) {
                _displayMessageLiveData.postValue(R.string.msg_error_missing_schools)
            }
            _schoolsLiveData.postValue(schools)
        }
    }

    /**
     * Used to clear the school score from the view, needs to be run on the main UI thread before
     * the user is presented with a new set of scores.
     */
    fun clearSchoolScore() {
        _schoolScoreLiveData.value = null
    }

    /**
     * Clear the display message observable after it is handled
     */
    fun clearDisplayMessage() {
        _displayMessageLiveData.value = null
    }

    /**
     * Used to fetch a set of scores for the school
     *
     * @param dbn - A unique school identifier
     */
    fun fetchSchoolScore(dbn: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val schoolScore = schoolScoreRepository.getSchoolScore(dbn)
            Timber.i("School Score: $schoolScore")
            if (schoolScore == null) {
                _displayMessageLiveData.postValue(R.string.msg_error_missing_scores)
            }
            _schoolScoreLiveData.postValue(schoolScore)
        }
    }
}
