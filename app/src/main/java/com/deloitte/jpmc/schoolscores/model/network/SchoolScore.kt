package com.deloitte.jpmc.schoolscores.model.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SchoolScore(
    val dbn: String,
    @Json(name = "school_name")
    val schoolName: String,
    @Json(name = "num_of_sat_test_takers")
    val testTakerCount: String,
    @Json(name = "sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String,
    @Json(name = "sat_math_avg_score")
    val satMathAvgScore: String,
    @Json(name = "sat_writing_avg_score")
    val satWritingAvgScore: String,
)
