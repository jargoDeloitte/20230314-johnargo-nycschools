package com.deloitte.jpmc.schoolscores

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

/**
 * Used for initialization of top-level state for the application.
 */
@HiltAndroidApp
class SchoolScoresApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
