package com.deloitte.jpmc.schoolscores.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.deloitte.jpmc.schoolscores.databinding.FragmentSchoolsBinding
import com.deloitte.jpmc.schoolscores.viewmodel.SchoolScoreViewModel
import timber.log.Timber

/**
 * Displays the list of Schools
 */
class SchoolsFragment : Fragment() {

    private lateinit var binding: FragmentSchoolsBinding
    private val viewModel: SchoolScoreViewModel by activityViewModels()
    private var schoolsAdapter: SchoolsAdapter = SchoolsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentSchoolsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvSchoolList.adapter = schoolsAdapter
        schoolsAdapter.setClickHandler { schoolName: String, dbn: String ->
            onSchoolSelected(
                schoolName,
                dbn,
            )
        }

        viewModel.clearSchoolScore()
        viewModel.schoolsLiveData.observe(viewLifecycleOwner) {
            schoolsAdapter.setData(it)
        }
    }

    override fun onDestroyView() {
        // Clear the adapter from the recycler view to make LeakCanary happy.
        val rv = binding.rvSchoolList
        rv.adapter = null

        super.onDestroyView()
    }

    /**
     * Used by a click handler for navigating to the School Scores
     *
     * @param schoolName - The name of the school selected
     * @param dbn - The unique identifier for the school
     */
    private fun onSchoolSelected(schoolName: String, dbn: String) {
        Timber.i("Selected school $dbn")
        val action = SchoolsFragmentDirections
            .actionSchoolListFragmentToSchoolScoreFragment(schoolName, dbn)
        findNavController().navigate(action)
    }
}
