package com.deloitte.jpmc.schoolscores.model.repository

import com.deloitte.jpmc.schoolscores.model.network.School
import com.deloitte.jpmc.schoolscores.model.network.SchoolScore
import com.deloitte.jpmc.schoolscores.model.network.SchoolScoreRemoteService
import timber.log.Timber
import javax.inject.Inject

/**
 * Used to provide the data to be consumed by upstream logic.
 *
 * @param schoolScoreRemoteService - The remote school score service
 */
@SuppressWarnings("ForbiddenComment")
class SchoolScoreRepository @Inject constructor(
    private val schoolScoreRemoteService: SchoolScoreRemoteService,
) {
    /**
     * Retrieve a list of schools
     *
     * @return The list of schools
     */
    @SuppressWarnings("TooGenericExceptionCaught")
    suspend fun getSchools(): List<School> {
        return try {
            // TODO: Fetch from a local service first
            val response = schoolScoreRemoteService.getSchools()
            if (response.isSuccessful) {
                response.body() ?: emptyList()
            } else {
                // TODO: Process Error Body and return a meaningful error message
                emptyList()
            }
        } catch (e: Exception) {
            // TODO: Return a meaningful error message.
            Timber.e(e)
            emptyList()
        }
    }

    /**
     * Retrieve the scores for a given school
     *
     * @param dbn - The unique ID for the school
     *
     * @return The school's set of scores
     */
    @SuppressWarnings("TooGenericExceptionCaught")
    suspend fun getSchoolScore(dbn: String): SchoolScore? {
        return try {
            // TODO: Fetch from a local service first
            val response = schoolScoreRemoteService.getSchoolScore(dbn)
            if (response.isSuccessful && response.body()?.isNotEmpty() == true) {
                response.body()?.first()
            } else {
                // TODO: Process Error Body and return a meaningful error message
                null
            }
        } catch (e: Exception) {
            // TODO: Return a meaningful error message.
            Timber.e(e)
            null
        }
    }
}
