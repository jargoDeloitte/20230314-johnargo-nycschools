package com.deloitte.jpmc.schoolscores.model.network

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Used to constrain the API call to a subset of fields to improve network performance
 */
class FilterFieldsInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return if (request.url().encodedPathSegments().last() == "s3k6-pzi2.json") {
            val urlBuilder = request.url().newBuilder()
            urlBuilder.addQueryParameter("\$select", School.queryParameterSchoolFilter.joinToString(","))
            val requestBuilder = request.newBuilder().url(urlBuilder.build())
            chain.proceed(requestBuilder.build())
        } else {
            chain.proceed(request)
        }
    }
}
