package com.deloitte.jpmc.schoolscores.model.network

import com.deloitte.jpmc.schoolscores.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Used to include our App Token on every API call.
 */
class TokenInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        response.header("X-App-Token", BuildConfig.API_TOKEN)

        return response
    }
}
