package com.deloitte.jpmc.schoolscores.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.deloitte.jpmc.schoolscores.model.network.School
import com.deloitte.jpmc.schoolscores.model.network.SchoolScore
import com.deloitte.jpmc.schoolscores.model.repository.SchoolScoreRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import junit.framework.TestCase.assertTrue
import junit.framework.TestCase.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class SchoolScoreViewModelTest {

    @get:Rule val rule = InstantTaskExecutorRule()
    private val repository: SchoolScoreRepository = mockk()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
    }

    @Test
    fun testGetSchools() {
        coEvery { repository.getSchools() } returns listOf(
            School(dbn = "123ABC", schoolName = "TestSchool"),
            School(dbn = "789XYZ", schoolName = "TestSchool 2"),
        )

        val subject = SchoolScoreViewModel(repository)
        subject.fetchSchools()

        val latch = CountDownLatch(1)
        val observer = Observer<List<School>> { value ->
            assertTrue(value.size == 2)
            latch.countDown()
        }
        subject.schoolsLiveData.observeForever(observer)

        if (!latch.await(5, TimeUnit.SECONDS)) {
            subject.schoolsLiveData.removeObserver(observer)
            fail("LiveData was not posted")
        }
    }

    @Test
    fun testGetSchoolScore() {
        coEvery { repository.getSchoolScore(any()) } returns
            SchoolScore("123ABC", "TestSchool", "10", "100", "200", "300")

        val subject = SchoolScoreViewModel(repository)
        subject.fetchSchoolScore("123ABC")

        val latch = CountDownLatch(1)
        val observer = Observer<SchoolScore?> { value ->
            assertNotNull(value)
            latch.countDown()
        }
        subject.schoolScoreLiveData.observeForever(observer)

        if (!latch.await(5, TimeUnit.SECONDS)) {
            subject.schoolScoreLiveData.removeObserver(observer)
            fail("LiveData was not posted")
        } else {
            // Test the clearing of School Score data
            subject.schoolScoreLiveData.removeObserver(observer)
            assertNotNull(subject.schoolScoreLiveData.value)
            subject.clearSchoolScore()
            assertNull(subject.schoolScoreLiveData.value)
        }
    }

    @Test
    fun testDisplayMessages() {
        coEvery { repository.getSchoolScore(any()) } returns null

        val subject = SchoolScoreViewModel(repository)
        subject.fetchSchoolScore("123ABC")
        val latch = CountDownLatch(1)
        val observer = Observer<Int?> { value ->
            assertNotNull(value)
            latch.countDown()
        }

        subject.displayMessageLiveData.observeForever(observer)

        if (!latch.await(5, TimeUnit.SECONDS)) {
            subject.displayMessageLiveData.removeObserver(observer)
            fail("LiveData was not posted")
        } else {
            // Test the clearing of Display Messages
            subject.displayMessageLiveData.removeObserver(observer)
            assertNotNull(subject.displayMessageLiveData.value)
            subject.clearDisplayMessage()
            assertNull(subject.displayMessageLiveData.value)
        }
    }
}
