package com.deloitte.jpmc.schoolscores.model.repository

import com.deloitte.jpmc.schoolscores.model.network.School
import com.deloitte.jpmc.schoolscores.model.network.SchoolScore
import com.deloitte.jpmc.schoolscores.model.network.SchoolScoreRemoteService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Response

@RunWith(JUnit4::class)
class SchoolScoreRepositoryTest {

    private lateinit var subject: SchoolScoreRepository
    private val remoteService: SchoolScoreRemoteService = mockk()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        subject = SchoolScoreRepository(remoteService)
    }

    @Test
    fun testGetSchools() = runBlocking {
        coEvery { remoteService.getSchools() } returns Response.success(
            listOf(
                School(dbn = "123ABC", schoolName = "TestSchool"),
                School(dbn = "789XYZ", schoolName = "TestSchool 2"),
            ),
        )
        val schools = subject.getSchools()
        assertNotNull(schools)
        assertTrue(schools.size == 2)
    }

    @Test
    fun testGetSchoolScore() = runBlocking {
        coEvery { remoteService.getSchoolScore(any()) } returns Response.success(
            listOf(
                SchoolScore("ABC123", "Test School 1", "10", "100", "200", "300"),
            ),
        )
        val schoolScore = subject.getSchoolScore("ABC123")
        assertNotNull(schoolScore)
        assertEquals(schoolScore?.dbn, "ABC123")
        assertEquals(schoolScore?.schoolName, "Test School 1")
        assertEquals(schoolScore?.testTakerCount, "10")
        assertEquals(schoolScore?.satCriticalReadingAvgScore, "100")
        assertEquals(schoolScore?.satMathAvgScore, "200")
        assertEquals(schoolScore?.satWritingAvgScore, "300")
    }
}
