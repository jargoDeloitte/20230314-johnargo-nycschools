package com.deloitte.jpmc.schoolscores.view

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.deloitte.jpmc.schoolscores.R
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolScoresTest {

    @Before
    fun setup() {
        launchActivity<MainActivity>()
    }

    @Test
    fun openApp() {
        onView(withId(R.id.frame_main)).check(matches(isDisplayed()))
    }
}
